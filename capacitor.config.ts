import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'Ionic2DoV6',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
