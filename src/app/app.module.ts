import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { TaskListPage } from '../pages/tasklist/tasklist';
//Imports AngularFire module to work with Firebase services
import { AngularFireModule} from '@angular/fire/compat'
//Imports AngularFire database module to work with Firebase realtime database service 
import { Dialogs } from '@ionic-native/dialogs/ngx';

export const firebaseConfig = {
  apiKey: "AIzaSyB_liXZypT3BUeADy1ZloarHZPYFvzK9P4",
  authDomain: "ionic2do-52d1c.firebaseapp.com",
  databaseURL: "https://ionic2do-52d1c-default-rtdb.firebaseio.com/",
  projectId: 'ionic2do-52d1c',
  storageBucket: "",
  messagingSenderId: "583960043142",
  appId: "583960043142:android:aa83c30f099dbf3df0fb07"
};

@NgModule({
  declarations: [
    AppComponent,
    TaskListPage
  ],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, AngularFireModule.initializeApp(firebaseConfig)],
  providers: [Dialogs, { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
  entryComponents: [
    AppComponent,
    //TaskListPage component registration for App loading
    TaskListPage
  ],
})
export class AppModule {}
