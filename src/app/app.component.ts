import { Component } from '@angular/core';
//Imports the TaskListPage component
import { TaskListPage } from '../pages/tasklist/tasklist'
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  //Defines the TaskListPage template as the root page
  rootPage: any = TaskListPage;
  constructor(platform: Platform) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
    });
  }
}
